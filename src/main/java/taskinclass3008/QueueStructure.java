package taskinclass3008;

public class QueueStructure {

    private Node head, tail;
    private int size;

    public QueueStructure() {
        this.head = null;
        this.tail = null;
        this.size = 0;
    }

    public boolean isEmpty(){
        return tail == null;
    }

    public boolean add(int data) {
        Node node = new Node(data);

        if(isEmpty()) head = tail = node;

        else {
            node.setNext(tail);
            tail = node;
        }
        size += 1;
        return true;
    }

    public int dequeue() {

        int removed = head.getData();
        if( !isEmpty() ) {
            searchThePenultimate(tail).setNext(null);
            head = searchThePenultimate(tail);
            size -= 1;
        }
        if(size == 0) head = tail = null;

        return removed;
    }

    private Node searchThePenultimate(Node node) {
        int counter = size;

        while (counter > 2) {
            node = node.getNext();
            counter --;
        }
        return node;
    }

    public static QueueStructure intercalar(QueueStructure list1, QueueStructure lis2) {
        QueueStructure queue = new QueueStructure();
        int counter = 1;
        while( !list1.isEmpty() || !lis2.isEmpty()) {
            if( counter%2 != 0 && !list1.isEmpty()) {
                queue.add(list1.dequeue());
            } else if (!lis2.isEmpty() ){
                queue.add(lis2.dequeue());
            }
            counter += 1;
        }
        return queue;
    }

    public String toString() {
        return "(" + size + ")" + "head = " + head + "\ntail = " + tail;
    }

    public static void main(String[] args) {
        /*System.out.println(a);
        System.out.println("\nDEQUEUE:");
        System.out.println(a.dequeue());
        System.out.println(a);
        System.out.println(a.dequeue());
        System.out.println(a);
        System.out.println(a.dequeue());
        System.out.println(a);
        System.out.println(a.dequeue());
        System.out.println(a);
        a.add(5);
        System.out.println(a);
        System.out.println("DEQUEUE: 5");
        System.out.println(a.dequeue());*/

        QueueStructure a = new QueueStructure();
        a.add(1);
        a.add(3);
        a.add(5);
        a.add(7);
        a.add(9);
        System.out.println(a);

        QueueStructure b = new QueueStructure();
        b.add(2);
        b.add(4);
        b.add(6);
        b.add(8);
        System.out.println(b);

        System.out.println(intercalar(a, b));

        System.out.println();

        a.add(2);
        a.add(4);
        a.add(6);
        a.add(8);

        b.add(1);
        b.add(3);
        b.add(5);
        b.add(7);
        b.add(9);

        System.out.println(intercalar(a, b));
    }
}
