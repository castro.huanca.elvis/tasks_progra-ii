package game0208;

public class Util {

    private static final int MINIMUM_POSITION = 0;

    public static boolean isWithinRange(int max, int number){
        return number > MINIMUM_POSITION && number < max;
    }

    public static int[] generateRandomPositions(String[][] board){
        int yMax = board.length;
        int xMax = board[0].length;
        int x, y;

        do {
            x = (int) (Math.random() * xMax);
            y = (int) (Math.random() * yMax);
        }while(!isWithinRange(yMax, y) || !isWithinRange(xMax, x));

        return new int[] {x, y};
    }

    public static int generateRandomPositionX(String[][] board){
        int xMax = board[0].length;
        int x;
        do{
            x = (int) (Math.random() * xMax);
        }while(!isWithinRange(xMax, x));
        return x;
    }

    public static int generateRandomPositionY(String[][] board){
        int yMax = board.length;
        int y;
        do{
            y = (int) (Math.random() * yMax);
        }while(!isWithinRange(yMax, y));

        return y;
    }

    public static boolean contains(int[][] board, int[] positions) {
        for (int[]iteratorArray : board) {
            if(iteratorArray[0] == positions[0] && iteratorArray[1] == positions[1]) return true;
        }
        return false;
    }
}
