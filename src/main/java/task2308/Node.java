package task2308;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class Node<T> {

    private T data;
    private Node<T> next;

    public Node(T data) {
        this.data = data;
        next = null;
    }

    public T getData() {
        return data;
    }

    public Node<T> getNext() {
        return next;
    }

    public void setNext(Node<T> node) {
        next = node;
    }

    public String toString() {
        return "{data=" + data + ", sig->" + next + "}";
    }
}