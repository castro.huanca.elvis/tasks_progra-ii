package task1508;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public interface Bag<T extends Comparable<T> > {
    boolean add(T data);
    void selectionSort();
    void bubbleSort();
    void xchange(int x, int y);
}

class Node<T> {
    private T data;
    private Node<T> next;
    public Node(T data) { this.data = data; }
    public T getData() { return data;}
    public Node<T> getNext() { return next; }
    public void setNext(Node<T> node) { next = node; }
    public String toString() {
        return "{data=" + data + ", sig->" + next + "}";
    }
}
class LinkedBag<T extends Comparable<T> > implements Bag<T> {
    private int size;
    private Node<T> root;
    public boolean add(T data) {
        Node<T> node = new Node<>(data);
        node.setNext(root);
        root = node;
        size++;
        return true;
    }
    public String toString() {
        return "(" + size + ")" + "root=" + root;
    }

    //TASK
    public void selectionSort(){
        for (int i = 0; i < size - 1; i++) {
            for (int j = i + 1; j < size; j++) {
                if (getNode(i).getData().compareTo(getNode(j).getData()) > 0) {
                    xchange(i, j);
                }
            }
        }
    }

    public void bubbleSort() {
        boolean sorted;
        do {
            sorted = true;
            for (int i = 0; i < size -1; i++) {
                if(getNode(i).getData().compareTo(getNode(i + 1).getData()) > 0){
                    xchange(i, i + 1);
                    sorted = false;
                }
            }
        } while(!sorted);
    }

    @Override
    public void xchange(int x, int y) {
        if (x == y)
            return;
        Node<T> previousX = null, currentX = root;
        int i = 0;
        while (currentX != null && i != x) {
            previousX = currentX;
            currentX = currentX.getNext();
            i++;
        }
        Node<T> previousY = null, currentY = root;
        i = 0;
        while (currentY != null && i != y) {
            previousY = currentY;
            currentY = currentY.getNext();
            i++;
        }
        if (currentX == null || currentY == null)
            return;
        if (previousX != null)
            previousX.setNext(currentY);
        else
            root = currentY;
        if (previousY != null)
            previousY.setNext(currentX);
        else
            root = currentX;
        Node<T> temp = currentX.getNext();
        currentX.setNext(currentY.getNext());
        currentY.setNext(temp);
    }

    private Node<T> getNode(int index) {
        Node<T> nodeAux = root;
        while(index > 0) {
            nodeAux = nodeAux.getNext();
            index --;
        }
        return nodeAux;
    }

}