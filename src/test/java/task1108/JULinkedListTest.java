package task1108;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class JULinkedListTest {

    JULinkedList<String> strings = new JULinkedList<>();
    JULinkedList<Integer> integers = new JULinkedList<>();

    private void fillStrings() {
        strings.add("One");
        strings.add("Two");
        strings.add("Three");
        strings.add("Four");
        strings.add("Five");
        strings.add("One");
        strings.add("Two");
        strings.add("Three");
    }

    private void fillIntegers() {
        integers.add(1);
        integers.add(2);
        integers.add(3);
        integers.add(4);
        integers.add(5);
        integers.add(1);
        integers.add(2);
        integers.add(3);
    }

    @Test
    void sizeTest() {
        fillStrings();
        assertEquals(8, strings.size());

        fillIntegers();
        assertEquals(8, integers.size());
    }

    @Test
    void isEmptyTest() {
        assertTrue(strings.isEmpty());
        assertTrue(integers.isEmpty());

        fillStrings();
        assertFalse(strings.isEmpty());

        fillIntegers();
        assertFalse(integers.isEmpty());
    }

    @Test
    void iteratorHasNextTest() {
        fillStrings();

        assertTrue(strings.iterator().hasNext());
        assertTrue(strings.iterator().hasNext());
        assertTrue(strings.iterator().hasNext());
        assertTrue(strings.iterator().hasNext());
        assertTrue(strings.iterator().hasNext());
        assertTrue(strings.iterator().hasNext());
        assertTrue(strings.iterator().hasNext());

        assertFalse(strings.iterator().hasNext());

    }

    @Test
    void iteratorNextTest() {
        fillStrings();

        assertEquals("Two", strings.iterator().next());
        assertEquals("Three", strings.iterator().next());
        assertEquals("Four", strings.iterator().next());
        assertEquals("Five", strings.iterator().next());

    }

    @Test
    void addTest() {
        assertTrue(strings.add("Hello"));
        assertTrue(strings.add("Bye"));
        assertTrue(strings.add("A"));

        assertEquals(3, strings.size());

        assertTrue(integers.add(1));
        assertTrue(integers.add(2));
        assertTrue(integers.add(3));
        assertTrue(integers.add(4));

        assertEquals(4, integers.size());
    }

    @Test
    void removeForObjectTest() {
        fillStrings();
        assertTrue(strings.remove("One"));
        assertTrue(strings.remove("One"));
        assertFalse(strings.remove("One"));

        assertTrue(strings.remove("Three"));
        assertTrue(strings.remove("Three"));
        assertFalse(strings.remove("Three"));

        assertTrue(strings.remove("Five"));
        assertFalse(strings.remove("Five"));

        assertEquals(3, strings.size());


        fillIntegers();

        Integer integer1 = 1;
        Integer integer3 = 3;
        Integer integer5 = 5;

        assertTrue(integers.remove(integer1));
        assertTrue(integers.remove(integer1));
        assertFalse(integers.remove(integer1));

        assertTrue(integers.remove(integer3));
        assertTrue(integers.remove(integer3));
        assertFalse(integers.remove(integer3));

        assertTrue(integers.remove(integer5));
        assertFalse(integers.remove(integer5));

        assertEquals(3, integers.size());
    }

    @Test
    void clearTest() {
        fillStrings();
        assertFalse(strings.isEmpty());
        strings.clear();
        assertEquals(0, strings.size());
        assertTrue(strings.isEmpty());
        assertNull(strings.getRootForTest());

        fillIntegers();
        assertFalse(integers.isEmpty());
        integers.clear();
        assertEquals(0, integers.size());
        assertTrue(integers.isEmpty());
        assertNull(integers.getRootForTest());
    }

    @Test
    void getTest() {
        fillStrings();
        assertEquals("One", strings.get(0));
        assertEquals("Three", strings.get(2));
        assertEquals("Five", strings.get(4));

        fillIntegers();
        assertEquals(1, integers.get(0));
        assertEquals(3, integers.get(2));
        assertEquals(5, integers.get(4));
    }

    @Test
    void removeForIndexTest() {
        fillStrings();

        assertEquals("One", strings.remove(0));
        assertEquals(7, strings.size());

        assertEquals("Two", strings.remove(0));
        assertEquals(6, strings.size());

        assertEquals("Three", strings.remove(0));
        assertEquals(5, strings.size());

        assertEquals("Five", strings.remove(1));
        assertEquals(4, strings.size());

        fillIntegers();

        assertEquals(1, integers.remove(0));
        assertEquals(7, integers.size());

        assertEquals(2, integers.remove(0));
        assertEquals(6, integers.size());

        assertEquals(3, integers.remove(0));
        assertEquals(5, integers.size());

        assertEquals(5, integers.remove(1));
        assertEquals(4, integers.size());
    }

    @Test
    void containsTest() {
        assertFalse(strings.contains("ITEM"));
        assertFalse(integers.contains(12));

        fillStrings();
        assertTrue(strings.contains("One"));
        assertTrue(strings.contains("Two"));
        assertTrue(strings.contains("Five"));

        fillIntegers();
        assertTrue(integers.contains(1));
        assertTrue(integers.contains(2));
        assertTrue(integers.contains(5));
    }

    @Test
    void setTest() {
        fillStrings();

        strings.set(0, "new 0");
        assertEquals("new 0", strings.get(0));
        strings.set(1, "new 1");
        assertEquals("new 1", strings.get(1));
        strings.set(4, "new 4");
        assertEquals("new 4", strings.get(4));
        strings.set(7, "new 7");
        assertEquals("new 7", strings.get(7));

        fillIntegers();

        integers.set(0, 10);
        assertEquals(10, integers.get(0));
        integers.set(2, 12);
        assertEquals(12, integers.get(2));
        integers.set(4, 14);
        assertEquals(14, integers.get(4));
        integers.set(6, 16);
        assertEquals(16, integers.get(6));

    }

    @Test
    void indexOfTest() {
        fillStrings();
        assertEquals(0, strings.indexOf("One"));
        assertEquals(2, strings.indexOf("Three"));
        assertEquals(4, strings.indexOf("Five"));

        fillIntegers();
        assertEquals(1, integers.indexOf(2));
        assertEquals(2, integers.indexOf(3));
        assertEquals(3, integers.indexOf(4));
    }

    @Test
    void lastIndexOfTest() {
        fillStrings();
        assertEquals(5, strings.lastIndexOf("One"));
        assertEquals(6, strings.lastIndexOf("Two"));
        assertEquals(7, strings.lastIndexOf("Three"));

        fillIntegers();
        assertEquals(5, integers.lastIndexOf(1));
        assertEquals(6, integers.lastIndexOf(2));
        assertEquals(7, integers.lastIndexOf(3));
    }

    //OPTIONAL
    @Test
    void addIndexTest() {
        fillStrings();
        assertEquals(8, strings.size());

        strings.add(0, "New 0");
        assertEquals("New 0", strings.get(0));
        assertEquals("One", strings.get(1));
        assertEquals(9, strings.size());

    }

    @Test
    void toArrayTest() {
        fillStrings();

        Object[] arrayString = strings.toArray();

        assertEquals("One", arrayString[0]);
        assertEquals("Two", arrayString[1]);
        assertEquals("Four", arrayString[3]);
        assertEquals("One", arrayString[5]);

        fillIntegers();

        Object[] arrayInteger = integers.toArray();

        assertEquals(1, arrayInteger[0]);
        assertEquals(2, arrayInteger[1]);
        assertEquals(4, arrayInteger[3]);
        assertEquals(1, arrayInteger[5]);

    }

    @Test
    void toArrayT1Test() {
        fillIntegers();;
        Integer[] integersArray = new Integer[integers.size()];
        integersArray = integers.toArray(integersArray);

        assertEquals(1, integersArray[0]);
        assertEquals(2, integersArray[1]);
        assertEquals(3, integersArray[2]);
        assertEquals(4, integersArray[3]);

        fillStrings();
        String[] stringsArray = new String[integers.size()];
        stringsArray = strings.toArray(stringsArray);

        assertEquals("One", stringsArray[0]);
        assertEquals("Two", stringsArray[1]);
        assertEquals("Three", stringsArray[2]);
        assertEquals("Four", stringsArray[3]);
    }

    private void fillString() {
        strings.add("One");
        strings.add("Two");
        strings.add("Three");
        strings.add("Four");
        strings.add("Five");
        strings.add("One");
        strings.add("Two");
        strings.add("Three");
    }

    private void fillIntegrs() {
        integers.add(1);
        integers.add(2);
        integers.add(3);
        integers.add(4);
        integers.add(5);
        integers.add(1);
        integers.add(2);
        integers.add(3);
    }
    @Test
    void subListTest() {

       fillStrings();
       List<String> subListStrings = strings.subList(2, 5);
       assertEquals(4, subListStrings.size());
       assertEquals("Three", subListStrings.get(0));
       assertEquals("Four", subListStrings.get(1));
       assertEquals("Five", subListStrings.get(2));
       assertEquals("One", subListStrings.get(3));

       fillIntegers();
       List<Integer> subListIntegers = integers.subList(0, 2);
       assertEquals(1, subListIntegers.get(0));
       assertEquals(2, subListIntegers.get(1));
       assertEquals(3, subListIntegers.get(2));

    }
}
