package task2808.exceptions;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class SyntaxException extends RuntimeException{

    public SyntaxException(String message) {
        super(message);
    }

}
