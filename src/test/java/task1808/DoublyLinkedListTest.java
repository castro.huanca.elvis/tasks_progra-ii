package task1808;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class DoublyLinkedListTest {

    DoublyLinkedList<Integer> integers = new DoublyLinkedList<>();
    DoublyLinkedList<String> strings = new DoublyLinkedList<>();

    @Test
    void addTest() {

        assertTrue(integers.isEmpty());
        assertEquals(0, integers.size());

        integers.add(1);
        integers.add(2);
        integers.add(3);
        integers.add(4);
        integers.add(5);
        integers.add(6);

        assertEquals(6, integers.size());

        integers.add(7);
        assertEquals(7, integers.size());

        assertFalse(integers.isEmpty());

        assertTrue(strings.isEmpty());
        assertEquals(0, strings.size());

        strings.add("one");
        strings.add("two");
        strings.add("three");
        assertEquals(3, strings.size());

        strings.add("four");
        strings.add("five");
        strings.add("six");
        assertEquals(6,strings. size());

        assertFalse(strings.isEmpty());
    }

    @Test
    void isEmptyTest() {
        assertTrue(integers.isEmpty());

        integers.add(123);
        assertFalse(integers.isEmpty());

        integers.remove(123);
        assertTrue(integers.isEmpty());

        integers.add(143);
        assertFalse(integers.isEmpty());

        assertTrue(strings.isEmpty());

        strings.add("one");
        assertFalse(strings.isEmpty());

        assertTrue(strings.remove("one"));
        assertTrue(strings.isEmpty());

    }

    @Test
    void sizeTest() {
        assertEquals(0, integers.size());

        integers.add(11);
        integers.add(12);
        integers.add(13);
        integers.add(14);
        integers.add(15);

        assertEquals(5, integers.size());

        integers.add(16);
        integers.add(17);
        integers.add(18);
        integers.add(19);

        assertEquals(9, integers.size());

        integers.remove(11);
        integers.remove(12);
        integers.remove(13);
        integers.remove(14);

        assertEquals(5, integers.size());

        integers.remove(19);
        assertEquals(4, integers.size());

        assertEquals(0, strings.size());

        strings.add("one");
        strings.add("two");
        strings.add("three");
        assertEquals(3, strings.size());

        strings.add("four");
        strings.add("five");
        strings.add("six");
        assertEquals(6, strings.size());

        strings.remove("one");
        strings.remove("two");
        strings.remove("three");
        assertEquals(3, strings.size());

        strings.add("ten");
        assertEquals(4, strings.size());
    }

    private void fillIntegers() {
        integers.add(1);
        integers.add(2);
        integers.add(3);
        integers.add(4);
        integers.add(5);
        integers.add(6);
        integers.add(7);
        integers.add(8);
        integers.add(9);
        integers.add(10);
    }

    private void fillStrings() {
        strings.add("a");
        strings.add("b");
        strings.add("c");
        strings.add("d");
        strings.add("e");
        strings.add("f");
        strings.add("g");
        strings.add("h");
        strings.add("i");
        strings.add("j");
    }

    @Test
    void getTest() {

        fillIntegers();

        assertEquals(3, integers.get(2));
        assertEquals(5, integers.get(4));
        assertEquals(10, integers.get(9));

        integers.remove(3);
        assertNull(integers.get(9));

        assertEquals(4, integers.get(2));
        assertEquals(5, integers.get(3));

        integers.add(14);
        assertEquals(14, integers.get(9));

        fillStrings();

        assertEquals("a", strings.get(0));
        assertEquals("c", strings.get(2));
        assertEquals("d", strings.get(3));
        assertEquals("g", strings.get(6));

        strings.remove("a");
        strings.remove("c");
        strings.remove("d");
        strings.remove("g");

        assertEquals("b", strings.get(0));
        assertEquals("f", strings.get(2));

    }

    @Test
    void removeTest() {

        fillIntegers();

        assertEquals(10, integers.size());
        assertEquals(10, integers.get(9));
        assertEquals(6, integers.get(5));

        assertTrue(integers.remove(6));
        assertFalse(integers.remove(6));

        assertEquals(9, integers.size());
        assertEquals(7, integers.get(5));

        assertEquals(10, integers.get(8));
        assertTrue(integers.remove(10));
        assertFalse(integers.remove(10));

        assertEquals(8, integers.size());
        assertNull(integers.get(8));

        assertFalse(integers.remove(123241));
        assertFalse(integers.remove(-121));
        assertFalse(integers.remove(0));

        fillStrings();

        assertEquals(10, strings.size());
        assertEquals("j", strings.get(9));
        assertEquals("f", strings.get(5));

        assertTrue(strings.remove("f"));
        assertFalse(strings.remove("f"));

        assertEquals(9, strings.size());
        assertEquals("g", strings.get(5));

        assertEquals("j", strings.get(8));
        assertTrue(strings.remove("j"));
        assertFalse(strings.remove("j"));

        assertEquals(8, strings.size());
        assertNull(strings.get(8));

        assertFalse(strings.remove("ONE"));
        assertFalse(strings.remove("TWO"));
        assertFalse(strings.remove("THREE"));

    }

    @Test
    void mixAddRemove() {
        Integer integer1 = 1;
        Integer integer2 = 2;
        Integer integer3 = 3;
        Integer integer4 = 4;
        Integer integer5 = 5;
        Integer integer6 = 6;
        Integer integer7 = 7;

        assertTrue(integers.add(integer1));
        assertTrue(integers.add(integer2));

        assertTrue(integers.remove(1));

        assertTrue(integers.add(integer3));
        assertTrue(integers.remove(2));

        assertTrue(integers.remove(3));

        assertTrue(integers.add(integer4));
        assertTrue(integers.remove(4));

        assertTrue(integers.add(integer5));
        assertTrue(integers.remove(5));

        assertTrue(integers.add(integer6));
        assertTrue(integers.remove(6));

        assertTrue(integers.add(integer7));
        assertTrue(integers.remove(7));
    }

    @Test
    void xchangeTest() {

        fillStrings();

        assertEquals("a", strings.get(0));
        assertEquals("j", strings.get(9));

        strings.xchange(0, 9);
        assertEquals("j", strings.get(0));
        assertEquals("a", strings.get(9));

        strings.xchange(9, 0);
        assertEquals("a", strings.get(0));
        assertEquals("j", strings.get(9));

        assertEquals("c", strings.get(2));
        assertEquals("f", strings.get(5));

        strings.xchange(5, 2);
        assertEquals("f", strings.get(2));
        assertEquals("c", strings.get(5));

    }

}
