package game0208;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class PlayerTest {

    Player player;
    String[][] board;

    private void fillBoard() {
        board = new String[10][10];
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                board[i][j] = "-";
            }
        }
    }

     @Test
    void moveHorizontallyTest() {
         fillBoard();

         player = new Player("Juan", 'A', 0, 1);

         assertEquals(0, player. moveHorizontally(board, Movement.LEFT));
         assertEquals(1, player.moveHorizontally(board, Movement.RIGHT));

         player.setPositionX(9);

         assertEquals(8, player.moveHorizontally(board, Movement.LEFT));
         assertEquals(9, player.moveHorizontally(board, Movement.RIGHT));
     }

     @Test
    void moveVerticallyTest() {
        fillBoard();

        player = new Player("Juan", 'A', 5, 0);

        assertEquals(0, player.moveVertically(board, Movement.UP));
        assertEquals(1, player.moveVertically(board, Movement.DOWN));

        player.setPositionY(9);

        assertEquals(8, player.moveVertically(board, Movement.UP));
        assertEquals(9, player.moveVertically(board, Movement.DOWN));
     }

     @Test
    void moveTest() {
        fillBoard();

        player = new Player("Juan", 'A', 5, 5);

        board = player.move(board, Player.LETTER_DOWN);
        assertEquals("A", board[6][5]);

        board = player.move(board, Player.LETTER_UP);
        assertEquals("A", board[5][5]);

         board = player.move(board, Player.LETTER_LEFT);
         assertEquals("A", board[5][4]);

         board = player.move(board, Player.LETTER_RIGHT);
         assertEquals("A", board[5][5]);

     }
}
