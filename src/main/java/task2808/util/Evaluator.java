package task2808.util;

import task2308.Stack;
import static task2808.util.Checker.invert;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class Evaluator {

    public static int convertCharsToInt(Stack<Character> digits) {

        String aux = "";
        int pos = digits.size() - 1;
        digits = invert(digits);
        while(pos >= 0) {
            aux = aux + digits.pop();
            pos --;
        }
        return Integer.parseInt(aux);
    }

}
