package game0208;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class Obstacle {

    public static final String LETTER_OBSTACLE = "X";

    private String[][] board;

    public Obstacle(String[][] board){
        this.board = board;
    }

    public String[][] insertObstacle(int x, int y){
        board[y][x] = LETTER_OBSTACLE;
        return board;
    }

    public String[][] generateObstacles(int numberOfObstacles){
        int[] positions;
        int[][] obstacles = new int[numberOfObstacles][2];
        int currentPosition = 0;
        while(numberOfObstacles > 0){
            positions = Util.generateRandomPositions(board);
            if(!Util.contains(obstacles, positions)){
                board = insertObstacle(positions[0], positions[1]);
                numberOfObstacles--;
                obstacles[currentPosition] = positions;
                currentPosition++;
            }
        }
        return board;
    }

}
