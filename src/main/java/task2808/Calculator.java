package task2808;

import task2308.Stack;
import task2808.operations.*;

import static task2808.util.Checker.*;
import static task2808.util.Evaluator.convertCharsToInt;
import static task2808.OperatorFiller.*;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class Calculator {

    private static final int MAX_VALUE_INT = 2147483647;

    private Stack<Character> exercise, operators;
    private Stack<Integer> numbers;

    public Calculator() {
        operators = new Stack<>();
        numbers = new Stack<>();
    }

    public int calculate(String exercise) {
        this.exercise = getTokens(exercise);
        separateItems();
        Calculable operator = identifyOperator();
        int number = 0;
        if(operator instanceof Multiplication || operator instanceof Factorial || operator instanceof Power) number = 1;
        while(numbers.size() > 0) {
            try {
                number = operator.perform(numbers.pop(), number);
            }catch (Exception e){
                System.out.println(e.getMessage());
                number = 0;
            }
        }
        return number;
    }

    private Calculable identifyOperator() {
        char operator = operators.pop();
        if(operator == ADDITION.getSymbol()) return ADDITION;
        if(operator == FACTORIAL.getSymbol()) return FACTORIAL;
        if(operator == MULTIPLICATION.getSymbol()) return MULTIPLICATION;
        if(operator == POWER.getSymbol()) return POWER;
        return null;
    }

    private static Stack<Character> getTokens(String str) {
        Stack<Character> tokens = new Stack<>();
        for (int i = 0; i < str.length(); i++) {
            tokens.push(str.charAt(i));
        }
        return tokens;
    }

    private void separateItems() {
        Stack<Character> digits = new Stack<>();
        Stack<Character> characters = invert(exercise);
        char currentCharacter = characters.pop();
        int value;
        while(characters.size() > 0) {
            if( isDigit(currentCharacter) ){
                digits.push(currentCharacter);
                currentCharacter = characters.pop();
                continue;
            }
            else if(isOperator(currentCharacter)) operators.push(currentCharacter);
            value = convertCharsToInt(digits);
            numbers.push(value);
            digits = clearStack(digits);

            currentCharacter = characters.pop();
        }
        value = convertCharsToInt(digits);
        numbers.push(value);
    }

    private Stack<Character> clearStack(Stack<Character> digits) {
        while(digits.size() > 0) digits.pop();
        return digits;
    }

}