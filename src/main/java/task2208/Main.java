package task2208;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class Main {
    public static void main(String[] args) {
        // 1.- sacar screenshot de la salida del metodo
        // 2.- sacar screenshot, del callStack (Debug) con un breakpoint en LlamadasEncadenadas.doFour
        LlamadasEncadenadas.doOne();

        // descomentar la llamda a: Infinito.whileTrue()
        // 1.- sacar screenshot de la exception
        // 2.- a continuacion describir el problema:
        /* Descripcion:
        Este método es infinito porque sufre de una recursvidad, al llamarse a sí mismo
        indeterminadas veces. Esto es debido a que no hay momento en el cuál se detenga,
        no hay condición alguna que detenga esta llamada al método, por lo tanto, se
        llama a sí mismo infinitas veces.
         */

        // 3.- volver a comentar la llmanda a: Infinito.whileTrue()
        //Infinito.whileTrue();

        // descomentar la llamda a: Contador.contarHasta(), pasnado un humer entero positivo
        // 1.- sacar screenshot de la salida del metodo
        // 2.- a continuacion describir como funciona este metodo:
        /* Descripcion:
        El método "contarHasta" recibe un parámetro que es un dato de tipo int.
        Este dato le sirve para imprimir en consola el mismo numero y sus menores hasta llegar
        al cero. En este método sucede una recursividad, ya que, se llama a sí mismo, pero esto
        no sucede de forma infinita, debido a que, con la condicional "if" se verifica que se siga
        llamando al método sólo cuando el valor que nos pasó como parámetro sea mayor que cero (en
        cada llamada al método este valor disminuye en 1). Por cada iteración o llamada a este método
        se muestra en consola el valor del contador (el cuál es el valor que le pasamos como parámetro).
         */
        Contador.contarHasta(5);

        System.out.println(RecursividadvsIteracion.factorialIter(5));
        System.out.println(RecursividadvsIteracion.factorialRecu(5));


    }
}
