package task1808;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class DoublyLinkedList<T extends Comparable<T>> implements List<T>{

    private int size;
    private Node<T> head, end;

    public DoublyLinkedList() {
        this.head = null;
        this.end = null;
    }

    //TASK
    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public boolean add(T data) {

        if(end == null) {
            head = end = new Node<>(data);
        }
        else {
            Node<T> nodeAux = new Node<>(data);
            end.setNext(nodeAux);
            nodeAux.setPrevious(end);
            end = nodeAux;
        }
        size += 1;
        return true;
    }

    @Override
    public T get(int index) {

        if (index >= size) return null;

        Node<T> nodeAux = head;
        while (index > 0) {
            nodeAux = nodeAux.getNext();
            index --;
        }
        return nodeAux.getData();
    }

    @Override
    public boolean remove(T data) {
        if(!contains(data)) return false;

        Node<T> nodeAux = head;
        boolean find = false;
        while(nodeAux != null && !find){
            find = (nodeAux.getData().equals(data));
            if(!find) {
                nodeAux = nodeAux.getNext();
            }
        }
        if(find) {
            if(nodeAux.equals(head)) head = nodeAux.getNext();

            else {
                nodeAux.getPrevious().setNext(nodeAux.getNext());
                if(nodeAux.getNext() != null) nodeAux.getNext().setPrevious(nodeAux.getPrevious());
            }
        }
        size -= 1;
        if(size == 0) head = end = null;
        return true;
    }

    private boolean contains(T data) {
        Node<T> nodeAux = head;
        while(nodeAux != null) {
            if(nodeAux.getData().equals(data)) return true;
            nodeAux = nodeAux.getNext();
        }
        return false;
    }

    //TASK 19-08
    public void xchange(int x, int y) {

        if (x == y) return;
        Node<T> previousX = null, currentX = head;
        int i = 0;
        while (currentX != null && i != x) {
            previousX = currentX;
            currentX = currentX.getNext();
            i++;
        }

        Node<T> previousY = null, currentY = head;
        i = 0;
        while (currentY != null && i != y) {
            previousY = currentY;
            currentY = currentY.getNext();
            i++;
        }
        if (currentX == null || currentY == null) return;
        else {
            currentX.setPrevious(previousY);
            currentY.setPrevious(previousX);
        }

        if (previousX != null)  previousX.setNext(currentY);

        else head = currentY;

        if (previousY != null) previousY.setNext(currentX);

        else head = currentX;

        Node<T> temp = currentX.getNext();
        currentX.setNext(currentY.getNext());
        currentY.setNext(temp);
    }





    //NO
    @Override
    public void selectionSort() {

    }

    @Override
    public void bubbleSort() {

    }

    @Override
    public void mergeSort() {

    }

}
