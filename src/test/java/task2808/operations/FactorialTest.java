package task2808.operations;

import org.junit.jupiter.api.Test;
import task2808.Calculator;

import static org.junit.jupiter.api.Assertions.assertEquals;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class FactorialTest {

    Calculator calculator = new Calculator();

    @Test
    void resolveFactorialTest() {

        int expected = 24;
        int actual = calculator.calculate("4!1 ") ;

        assertEquals(expected, actual);

        expected = 40320;
        actual = calculator.calculate("8!1 ") ;

        assertEquals(expected, actual);

        expected = 3628800;
        actual = calculator.calculate("10!1 ");

        assertEquals(expected, actual);
    }
}
