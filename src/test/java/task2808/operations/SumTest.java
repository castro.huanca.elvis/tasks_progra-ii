package task2808.operations;

import org.junit.jupiter.api.Test;
import task2808.Calculator;

import static org.junit.jupiter.api.Assertions.assertEquals;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class SumTest {

    Calculator calculator = new Calculator();

    @Test
    void twoOperantsSumTest() {

        int expected = 2;
        int actual = calculator.calculate("1+1 ");

        assertEquals(expected, actual);

        expected = 25;
        actual = calculator.calculate("24+1 ");

        assertEquals(expected, actual);

    }

    @Test
    void severalOperationsSumTest() {

        int expected = 25;
        int actual = calculator.calculate("1+1+1+1+1+1" +
                "+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1 ");

        assertEquals(expected, actual);

        expected = 50;
        actual = calculator.calculate("2+2+2+2+2+2" +
                "+2+2+2+2+2+2+2+2+2+2+2+2+2+2+2+2+2+2+2 ");

        assertEquals(expected, actual);

    }


}
