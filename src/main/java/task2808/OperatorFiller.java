package task2808;

import task2308.Stack;
import task2808.operations.*;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class OperatorFiller {

    public static final Calculable ADDITION = new Addition();
    public static final Calculable MULTIPLICATION = new Multiplication();
    public static final Calculable POWER = new Power();
    public static final Calculable FACTORIAL = new Factorial();
    public static final Stack<Character> OPERATORS = fillOperators();

    private static Stack<Character> fillOperators() {
        Stack<Character> operators = new Stack<>();
        operators.push('*');
        operators.push('+');
        operators.push('^');
        operators.push('!');
        return operators;
    }
}
