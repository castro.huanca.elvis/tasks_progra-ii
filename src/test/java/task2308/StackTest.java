package task2308;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class StackTest {

    Stack<Integer> stackInteger = new Stack<>();
    Stack<String> stackString = new Stack<>();

    @Test
    void pushIntTest() {

        assertTrue(stackInteger.isEmpty());

        stackInteger.push(3);
        stackInteger.push(4);
        stackInteger.push(1);
        stackInteger.push(3);
        stackInteger.push(6);
        stackInteger.push(5);
        stackInteger.push(8);
        stackInteger.push(9);
        stackInteger.push(3);
        stackInteger.push(31);
        stackInteger.push(1);

        assertEquals(11, stackInteger.size());
    }

    @Test
    void pushStringTest() {

        assertTrue(stackString.isEmpty());

        stackString.push("One");
        stackString.push("Two");
        stackString.push("Three");
        stackString.push("Four");
        stackString.push("Five");
        stackString.push("Six");
        stackString.push("Seven");

        assertEquals(7, stackString.size());
    }

    @Test
    void popIntTest() {

        stackInteger.push(1);
        stackInteger.push(2);
        stackInteger.push(3);
        stackInteger.push(4);
        stackInteger.push(5);
        stackInteger.push(6);
        stackInteger.push(7);
        stackInteger.push(8);
        stackInteger.push(9);
        stackInteger.push(10);

        stackInteger.pop();

        assertEquals(9, stackInteger.size());
    }

    @Test
    void popStringTest() {

        stackString.push("One");
        stackString.push("Two");
        stackString.push("Three");
        stackString.push("Four");
        stackString.push("Five");
        stackString.push("Six");
        stackString.push("Seven");

        stackString.pop();

        assertEquals(6, stackString.size());

    }
}
