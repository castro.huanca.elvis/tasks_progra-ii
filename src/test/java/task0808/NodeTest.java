package task0808;

import static org.junit.Assert.*;
import org.junit.Test;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class NodeTest {

    @Test
    public void test2() {
        Node n = new Node();
        n.data = 1337;
        n.next = new Node();
        n.next.data = 42;
        n.next.next = new Node();
        n.next.next.data = 23;
        try{
            int a = Node.getNth(n, 0);
            int b = Node.getNth(n, 1);
            int c = Node.getNth(n, 2);
            assertEquals(a, 1337);
            assertEquals(b, 42);
            assertEquals(c, 23);
        }catch(Exception e){
            assertTrue(false);
        }
    }

    @Test
    public void testNull() {
        try{
            Node.getNth(null, 0);
            assertTrue(false);
        }catch(Exception e){
            assertTrue(true);
        }
    }


    @Test
    public void testWrongIdx() {
        try{
            Node.getNth(new Node(), 1);
            assertTrue(false);
        }catch(Exception e){
            assertTrue(true);
        }
    }
}