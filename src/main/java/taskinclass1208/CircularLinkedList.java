package taskinclass1208;

//STUDENT: ELVIS JOSE CASTRO HUANCA
class Node<T> {
    private T data;
    private Node<T> next;
    public Node(T data) { this.data = data;}
    public T getData() { return data;}
    public Node<T> getNext() { return next; }
    public void setNext(Node<T> node) { next = node; }
    public String toString() {return "data=" + data;}
}
public class CircularLinkedList<T> implements List<T> {
    private Node<T> head;
    private int size;

    //Extra points
    public String toString() {
        Node<T> nodeAux = head;
        String listString = "";
        int repetitions = size;
        while(repetitions > 0) {
            if(repetitions == 1) listString += nodeAux;
            else {
                listString += (nodeAux + ", ");
                nodeAux = nodeAux.getNext();
            }
            repetitions --;
        }
        return "list = {" + listString + "}";
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    //task in class
    @Override
    public boolean add(T data) {
        Node<T> nodeAux = new Node<>(data);
        if (head == null) head = nodeAux;

        else searchTheLast(head).setNext(nodeAux);

        searchTheLast(head).setNext(head);

        size += 1;

        return true;
    }

    private Node<T> searchTheLast(Node<T> node){
        while (node.getNext() != null && node.getNext() != head) {
            node = node.getNext();
        }
        return node;
    }





    @Override
    public boolean remove(T data) {
        return false;
    }

    @Override
    public T get(int index) {
        if (isEmpty() || index < 0)
            return null;
        else {
            Node<T> node = head;
            for (int i = 0; i < index ; i++, node = node.getNext());

            return node.getData();
        }
    }

    public static List<Integer> dummyList(int s) {
        CircularLinkedList<Integer> l = new CircularLinkedList<>();
        l.size = s;
        l.head = new Node<>(l.size);
        dummyNode(l.head, --s, null);
        return l;
    }
    private static Node<Integer> dummyNode(Node<Integer> n, int d, Node<Integer> z) {
        z = z != null ? z : n;
        if (d <= 0) { n.setNext(z); return n;}
        else n.setNext(dummyNode(new Node<>(d), d -1, z));
        return  n;
    }
}
