package task2808.operations;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public interface Calculable {

    int perform(int left, int right);
    char getSymbol();
    int getPriority();

}
