package task1108;

import java.util.*;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class JULinkedList<T> implements List<T> {

    private Node<T> root;
    private static final int MINIMUM_POSITION = 0;

    @Override
    public int size() {
        int counter = 0;
        Node<T> nodeAux = root;
        while(nodeAux != null) {
            counter += 1;
            nodeAux = nodeAux.getNext();
        }
        return counter;
    }

    @Override
    public boolean isEmpty() {
        return root == null;
    }

    @Override
    public Iterator<T> iterator() {

        Iterator<T> iterator = new Iterator<>() {

            static int positionHasNext = 1;
            static int positionNext = 0;

            @Override
            public boolean hasNext() {
                if ( !isOutOfRange(positionHasNext) ){
                    positionHasNext += 1;
                    return true;
                }
                return false;
            }

            @Override
            public T next() {
                if(positionNext < size() - 1){
                    positionNext += 1;
                    return get(positionNext);
                }
                return null;
            }
        };
        return iterator;
    }

    private boolean isOutOfRange(int index) {
        return index >= size() || index < MINIMUM_POSITION;
    }

    @Override
    public boolean add(T t) {
        Node<T> nodeAux = new Node<>(t);
        if (root == null) root = nodeAux;

        else searchTheLast(root).setNext(nodeAux);

        return true;
    }

    private Node<T> searchTheLast(Node<T> node){
        while(node.getNext() != null){
            node = node.getNext();
        }
        return node;
    }

    @Override
    public boolean remove(Object o) {
        if(!contains(o)) return false;

        Node<T> previous = root;
        for (Node<T> current = root; current != null; previous = current, current = current.getNext()){
            if (current.getData().equals(o)){
                if (root == current) root = current.getNext();

                else previous.setNext(current.getNext());

                return true;
            }
        }
        return true;
    }

    @Override
    public void clear() {
        root = null;
    }

    @Override
    public T get(int index) {
        Node<T> nodeAux = root;
        while(index > 0) {
            nodeAux = nodeAux.getNext();
            index --;
        }
        return nodeAux.getData();
    }

    @Override
    public T remove(int index) {
        T nodeAux = get(index);
        remove(nodeAux);
        return nodeAux;
    }

    @Override
    public boolean contains(Object o) {
        Node<T> nodeAux = root;
        while(nodeAux != null){
            if(nodeAux.getData().equals(o)) return true;
            nodeAux = nodeAux.getNext();
        }
        return false;
    }

    @Override
    public T set(int index, T element) {
        Node<T> nodeAux = root;
        Node<T> previous = getPreviousNode(index, nodeAux);
        Node<T> current;
        if (index == 0) current = previous;

        else current = previous.getNext();

        current.setData(element);
        previous.setNext(current);
        root = previous;

        return current.getData();
    }

    private Node<T> getPreviousNode(int index, Node<T> node) {
        while(index > 0){
            node = node.getNext();
            index --;
        }
        return node;
    }

    @Override
    public int indexOf(Object o) {
        Node<T> nodeAux = root;
        int index = 0;
        while(nodeAux != null){
            if(nodeAux.getData().equals(o)) return index;
            index++;
            nodeAux = nodeAux.getNext();
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        Node<T> nodeAux = root;
        int currentIndex = 0, index = -1;
        while(nodeAux != null){
            if(nodeAux.getData().equals(o)) {
                index = currentIndex;
            }
            currentIndex ++;
            nodeAux = nodeAux.getNext();
        }
        return index;
    }



    //OPTIONAL
    @Override
    public void add(int index, T element) {
        Node<T> nodeAux = root;
        Node<T> previous = getPreviousNode(index, nodeAux);
        Node<T> current = new Node<>(element);
        if(index == 0){
            current.setNext(previous);
            previous = current;
        }

        else {
            current.setNext(previous.getNext());
            previous.setNext(current);
        }
        root = previous;
    }

    @Override
    public Object[] toArray() {
        Object[] array = new Object[size()];
        Node<T> nodeAux = root;
        for (int i = 0; i < size(); i++) {
            array[i] = nodeAux.getData();
            nodeAux = nodeAux.getNext();
        }
        return array;
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        Node<T> nodeAux = root;
        for (int i = 0; i < size(); i++) {
            a[i] = (T1) nodeAux.getData();
            nodeAux = nodeAux.getNext();
        }
        return a;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        JULinkedList<T> sublistFinal = new JULinkedList<>();
        for (int i = fromIndex; i <= toIndex; i++) {
            sublistFinal.add(get(i));
        }
        return sublistFinal;
    }



    //NO
    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }


    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    public Node getRootForTest() {
        return root;
    }
}
