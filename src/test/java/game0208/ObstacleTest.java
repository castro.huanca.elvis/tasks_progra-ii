package game0208;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class ObstacleTest {

    String[][] board = new String[10][10];
    Obstacle obstacle = new Obstacle(board);


    private void fillBoard(){
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                board[i][j] = "-";
            }
        }
    }

    @Test
    void insertObstacleTest(){
        int x = 2;
        int y = 2;

        fillBoard();

        board = obstacle.insertObstacle(x, y);

        assertEquals(Obstacle.LETTER_OBSTACLE, board[y][x]);
    }

    private int searchObstacles() {
        int counter = 0;
        for (String[] strings : board) {
            for (String string : strings) {
                if(string.equals(Obstacle.LETTER_OBSTACLE)){
                    counter += 1;
                }
            }
        }
        return counter;
    }

    @Test
    void generateObstaclesTest(){
        fillBoard();

        board = obstacle.generateObstacles(3);

        int numberOfObstaclesFound = searchObstacles();

        assertEquals(3, numberOfObstaclesFound);
    }

}
