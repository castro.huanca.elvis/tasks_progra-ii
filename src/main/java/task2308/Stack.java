package task2308;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class Stack<T extends Comparable<T> > {

    private Node<T> root;
    private int size;

    public Stack() {
        this.root = null;
        this.size = 0;
    }

    public void push(T x) {
        Node<T> node = new Node<>(x);
        node.setNext(root);
        root = node;
        this.size += 1;
    }

    public boolean isEmpty() {
        return root == null;
    }

    private T peek() {
        if (isEmpty()) {
            System.exit(1);
        }
        return root.getData();
    }

    public T pop(){
        if (isEmpty()) {
            System.exit(-1);
        }
        T top = peek();
        size -= 1;
        root = root.getNext();
        return top;
    }

    public int size() {
        return size;
    }

    @Override
    public String toString() {
        return "Stack{" +
                "root=" + root +
                ", count=" + size +
                '}';
    }
}