package taskinclass2508;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
class Student implements Comparable<Student> {
    String name;
    int score;
    public Student(String name, int score) {
        this.name = name;
        this.score = score;
    }
    public int compareTo(Student o) {
        int value;
        
        if(this.score > o.score) value = 1;
        else value = - 1 ;
        
        return value;
    }
    

    public String toString() {
        return "name:" + name + ", score: " + score;
    }
    
    
}
public class Main {
    public static void main(String[] args) {
        List<Student> list = new ArrayList<>();
        list.add(new Student("Std 2", 80));
        list.add(new Student("Std 4", 70));
        list.add(new Student("Std 3", 90));
        list.add(new Student("Std 1", 50));
        list.sort(Comparator.naturalOrder());
        for (Student s: list ) {
            System.out.println(s);
        }

        selectionSort(list);
    }

    public static void selectionSort(List<Student> list) {
        for (int i = 0; i < list.size() - 1; i++) {
            for (int j = 0; j < list.size(); j++) {
                if(list.get(i).compareTo(list.get(j)) > 0) {
                    Student aux = list.get(i);
                    list.set(i, list.get(j));
                    list.set(j, aux);
                }
            }
        }
    }


}