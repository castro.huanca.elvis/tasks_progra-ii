package task2808.exceptions;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class OperationException extends RuntimeException{

    public OperationException(String message) {
        super(message);
    }

}
