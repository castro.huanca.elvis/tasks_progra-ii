package taskinclass0209;

public class Queue<T extends Comparable<T>> {

    private Node<T> head, tail;
    private int size;

    public Queue() {
        this.head = null;
        this.tail = null;
        this.size = 0;
    }

    public boolean isEmpty(){
        return tail == null;
    }

    public boolean add(T data) {
        Node<T> node = new Node<>(data);

        if(contains(data)) return false;

        if(isEmpty()) head = tail = node;

        else {
            node.setNext(tail);
            tail = node;
        }
        size += 1;
        return true;
    }

    public boolean contains(T data) {
        int counter = size;
        Node<T> aux = tail;
        while (counter > 0) {
            if(aux.getData().compareTo(data) == 0) return true;
            aux = aux.getNext();
            counter --;
        }
        return false;
    }

    public T dequeue() {
        T removed = head.getData();
        if( !isEmpty() ) {
            searchThePenultimate(tail).setNext(null);
            head = searchThePenultimate(tail);
            size -= 1;
        }
        if(size == 0) head = tail = null;

        return removed;
    }

    public String toString() {
        return "(" + size + ")" + "head = " + head + "\ntail = " + tail;
    }

    private Node<T> searchThePenultimate(Node<T> node) {
        int counter = size;

        while (counter > 2) {
            node = node.getNext();
            counter --;
        }
        return node;
    }

    public static void main(String[] args) {
        Queue<Integer> a = new Queue<>();
        System.out.println(a.add(3));
        System.out.println(a.add(3));
        System.out.println(a.add(3));

        System.out.println();

        System.out.println(a.add(34));
        System.out.println(a.add(34));

        System.out.println();

        System.out.println(a.add(35));
        System.out.println(a.add(35));

        System.out.println(a);

        System.out.println(a.dequeue());

        System.out.println();

        System.out.println(a.add(3));

        System.out.println();

        System.out.println(a);

        System.out.println();
        Queue<String> b = new Queue<>();
        System.out.println(b.add("student1"));
        System.out.println(b.add("student1"));
        System.out.println();

        System.out.println(b.add("student2"));
        System.out.println(b.add("student2"));
        System.out.println();

        System.out.println(b.add("student3"));
        System.out.println(b.add("student3"));
        System.out.println(b.add("student3"));
        System.out.println(b.add("student3"));
        System.out.println();

        System.out.println(b);


    }
}
