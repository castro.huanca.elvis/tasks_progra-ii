package task2808.operations;

import task2808.exceptions.OperationException;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class Multiplication implements Calculable{

    @Override
    public int perform(int left, int right) {
        int  result = left * right;
        if(result < 0) {
            throw new OperationException("Out of range.");
        }
        return result;
    }

    @Override
    public char getSymbol() {
        return '*';
    }

    @Override
    public int getPriority() {
        return 1;
    }

}
