package task2808.operations;

import task2808.exceptions.OperationException;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class Addition implements Calculable{

    @Override
    public int perform(int left, int right) {
        int sum = left + right;
        if(sum < 0) {
            throw new OperationException("Out of range.");
        }
        return sum;
    }

    @Override
    public char getSymbol() {
        return '+';
    }

    @Override
    public int getPriority() {
        return 2;
    }

}
