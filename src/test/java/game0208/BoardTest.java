package game0208;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class BoardTest {

    Board board = new Board(10, 10);

    @Test
    void createBoardTest(){
        assertArrayEquals(new String[10][10], board.createBoard());
    }

    @Test
    void fillBoardTest(){
        String elementToFill = "_";
        String[][] actual = new String[10][10];

        actual = board.fillBoard(actual, elementToFill);

        assertEquals("_", actual[9][9]);
        assertEquals("_", actual[4][3]);
        assertEquals("_", actual[3][6]);
        assertEquals("_", actual[7][8]);
        assertEquals("_", actual[1][1]);
        assertEquals("_", actual[0][0]);
    }

    @Test
    void showBoardTest(){
        String[][] boardTest = new String[10][10];
        boardTest = board.fillBoard(boardTest, "-");
        String expected = " - - - - - - - - - -\n" +
                " - - - - - - - - - -\n" +
                " - - - - - - - - - -\n" +
                " - - - - - - - - - -\n" +
                " - - - - - - - - - -\n" +
                " - - - - - - - - - -\n" +
                " - - - - - - - - - -\n" +
                " - - - - - - - - - -\n" +
                " - - - - - - - - - -\n" +
                " - - - - - - - - - -\n";

        String actual = board.showBoard(boardTest);

        assertEquals(expected, actual);
    }





    @Test
    void insertPlayerTest() {
        int x = 3;
        int y = 3;

        String[][] boarTest = new String[10][10];

        boarTest = board.fillBoard(boarTest, "-");
        boarTest = board.insertPlayer(boarTest, 'A', x, y);

        String expected = " - - - - - - - - - -\n" +
                " - - - - - - - - - -\n" +
                " - - - - - - - - - -\n" +
                " - - - A - - - - - -\n" +
                " - - - - - - - - - -\n" +
                " - - - - - - - - - -\n" +
                " - - - - - - - - - -\n" +
                " - - - - - - - - - -\n" +
                " - - - - - - - - - -\n" +
                " - - - - - - - - - -\n";

        String actual = board.showBoard(boarTest);
        assertEquals(expected, actual);
    }
}
