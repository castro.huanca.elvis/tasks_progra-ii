package task0808;

//STUDENT: ELVIS JOSE CASTRO HUANCA
class Node2 {

    int data;
    Node2 next = null;

    Node2(final int data) {
        this.data = data;
    }

    public Node2(int data, Node2 next) {
        this.data = data;
        this.next = next;
    }

    public static Node2 append(Node2 listA, Node2 listB) {
        if( someIsNull(listA, listB) ) {
            return findTheDifferentFromNull(listA, listB);
        }
        else {
            searchTheLast(listA).next = listB;
        }
        return listA;
    }

    private static Node2 searchTheLast(Node2 list) {
        while(list.next != null){
            list = list.next;
        }
        return list;
    }

    private static Node2 findTheDifferentFromNull(Node2 listA, Node2 listB) {
        if(bothAreNull(listA, listB)){
            return null;
        }
        else if( isNull(listA) ) {
            return listB;
        }
        else {
            return listA;
        }
    }

    private static boolean bothAreNull(Node2 listA, Node2 listB){
        return listA == null && listB == null;
    }

    private static boolean isNull(Node2 list){
        return list == null;
    }

    private static boolean someIsNull(Node2 listA, Node2 listB) {
        return listA == null || listB == null;
    }

}