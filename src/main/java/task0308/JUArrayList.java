package task0308;

import java.util.*;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class JUArrayList implements List<Integer> {

    private Integer[] array;
    private static int counter;
    private static final int MINIMUM_POSITION = 0;

    public JUArrayList() {
        array = new Integer[0];
        counter = 0;
    }

    @Override
    public int size() {
        int counter = 0;
        for (Integer iteratorItem : array) {
            counter += 1;
        }
        return counter;
    }

    @Override
    public boolean isEmpty() {
        return !(array.length > 0);
    }

    @Override
    public boolean contains(Object o) {
        Integer object = (Integer) o;

        for (Integer iteratorObject : array) {
            if (object.equals(iteratorObject)) return true;
        }
        return false;
    }

    @Override
    public Iterator<Integer> iterator() {


        Iterator<Integer> iterator = new Iterator<>() {
            static int positionHasNext = 1;
            static int positionNext = 1;

            @Override
            public boolean hasNext() {
                if(!isOutOfRange(positionHasNext)){
                    positionHasNext++;
                    return true;
                }
                return false;
            }

            @Override
            public Integer next() {
                if(hasNext()){
                    Integer integer = array[positionNext];
                    positionNext++;
                    return integer;
                }
                return null;
            }
        };
        return iterator;
    }

    @Override
    public Object[] toArray() {
        return array;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        if(a.length < array.length){
            a = (T[]) new Object[array.length];
        }
        for (int i = 0; i < array.length; i++) {
            a[i] = (T) array[i];
        }
        return a;
    }

    @Override
    public boolean add(Integer integer) {
        if(counter == array.length){
            Integer[] arrayAux = new Integer[array.length + 1];
            for (int i = 0; i < array.length; i++) {
                if(!(array.length == 0)){
                    arrayAux[i] = array[i];
                }
            }
            array = arrayAux;
        }
        array[counter] = integer;
        counter++;
        return true;
    }

    @Override
    public boolean remove(Object o) {

        if (!(o instanceof Integer) || !contains(o)) {
            return false;
        }
        Integer index = indexToDelete( (Integer) o);
        Integer numberOfItems = array.length - (index + 1);
        Integer[] arrayAux = new Integer[numberOfItems];
        for (Integer i = index; i < array.length - 1; i++) {
            arrayAux[i] = array[i + 1];
        }
        array = arrayAux;
        return true;
    }

    private Integer indexToDelete(Integer integer){
        Integer index = null;
        for (int i = 0; i < array.length; i++) {
            if(integer.equals(array[i])){
                index = i;
            }
        }
        return index;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends Integer> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends Integer> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {
        array = new Integer[0];
    }

    @Override
    public Integer get(int index) {
        if(!isOutOfRange(index)){
            return array[index];
        }else{
            return null;
        }
    }

    public boolean isOutOfRange(int index){
        return index >= array.length || index < MINIMUM_POSITION;
    }

    @Override
    public Integer set(int index, Integer element) {
        array[index] = element;
        return array[index];
    }

    @Override
    public void add(int index, Integer element) {
        Integer[] arrayAux = new Integer[array.length + 1];

        for (int i = 0; i < array.length - 1; i++) {
            if(i < index){
                arrayAux[i] = array[i];
            }
            else if(i == index){
                arrayAux[i] = element;
            }
            else{
                arrayAux[i] = array[i + 1];
            }
        }
        array = arrayAux;
    }

    @Override
    public Integer remove(int index) {
        Integer itemToDelete = array[index];
        Integer numberOfItems = array.length - (index + 1);
        Integer[] arrayAux = new Integer[numberOfItems];
        for (int i = index; i < array.length - 1; i++) {
            arrayAux[i] = array[i + 1];
        }
        array = arrayAux;
        return itemToDelete;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<Integer> listIterator() {
        return null;
    }

    @Override
    public ListIterator<Integer> listIterator(int index) {
        return null;
    }


    @Override
    public List<Integer> subList(int fromIndex, int toIndex) {
        return null;
    }

}
