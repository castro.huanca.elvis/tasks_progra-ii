package game0208;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class Board {

    private int width, height;

    public Board(int width, int height){
        this.width = width;
        this.height = height;
    }

    public String[][] createBoard() {
        return new String[height][width];
    }

    public String[][] fillBoard(String[][] board, String elementToFill){
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                board[i][j] = elementToFill;
            }
        }
        return board;
    }

    public String showBoard(String[][] board){
        String endBoard = "";
        for (String[] strings : board) {
            for (String string : strings) {
                endBoard += " " + string;
            }
            endBoard += "\n";
        }
        return endBoard;
    }

    public String[][] insertPlayer(String[][] board, char nickname, int x, int y){

        if(board[y][x] != Obstacle.LETTER_OBSTACLE) {
            board[y][x] = String.valueOf(nickname);
        }
        return board;
    }

}
