package task2808.util;

import task2308.Stack;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class Checker {

    private static final int MAX_VALUE_INT = 2147483647;
    private static final int MIN_NUMBER_POSITIVE_INT = 0;

    public static Stack<Character> invert(Stack<Character> current) {
        Stack<Character> inverted = new Stack<>();
        while(current.size() > 0) {
            inverted.push(current.pop());
        }
        return inverted;
    }

    public static boolean isDigit(Character character) {
        return (character >= '0' && character <= '9');
    }

    public static boolean isOperator(Character c) {
        return (c == '+' || c == '*' || c == '^' || c == '!');
    }


    public boolean verifyRange(int number) {
        return number >= MIN_NUMBER_POSITIVE_INT && number < MAX_VALUE_INT;
    }

    public boolean verifyTypeVariable(String str) {
        int counter = 0;
        while(counter < str.length()) {
            if (str.charAt(counter) == '.' || str.charAt(counter) == ',') return false;
            counter ++;
        }
        return true;
    }

    public boolean verifyBrackets(String operation) {
        int countFirstBracket = 0;
        int countLastBracket = 0;
        char element, previousElement, nextElement;
        for (int i = 0; i < operation.length(); i++) {
            element = operation.charAt(i);
            if (i == operation.length()) nextElement = operation.charAt(i + 1);
            else nextElement = ' ';
            if (element == '(' && nextElement != ')') return true;
            if (i > 0) previousElement = operation.charAt(i - 1);
            else previousElement = ' ';
            if (element == ')' && previousElement != '(') return true;
            if (element == '(') countFirstBracket++;
            if (element == ')') countLastBracket++;
        }
        if (operation.charAt(0) == ')') return false;
        if (countFirstBracket == countLastBracket) return true;
        else return false;
    }

    public boolean verifyNotLetters(String operation){
        for (int i = 0; i < operation.length(); i++) {
            if ((operation.charAt(i) >= 'a' && operation.charAt(i) <= 'z') || (operation.charAt(i) >= 'A' && operation.charAt(i) <= 'Z')) {
                i = operation.length();
                return true;
            }
        }
        return false;
    }

}
