package task2808.util;

import org.junit.jupiter.api.Test;
import task2308.Stack;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static task2808.util.Evaluator.convertCharsToInt;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class EvaluatorTest {

    @Test
    void convertCharsToIntTest() {

        Stack<Character> digits = new Stack<>();
        digits.push('1');
        digits.push('1');
        digits.push('1');
        digits.push('1');
        digits.push('1');
        digits.push('1');

        int expected = 111111;
        int actual = convertCharsToInt(digits);

        assertEquals(expected, actual);

        digits.push('1');
        digits.push('1');
        digits.push('1');
        digits.push('1');
        digits.push('1');
        digits.push('1');
        digits.push('2');

        expected = 1111112;
        actual = convertCharsToInt(digits);

        assertEquals(expected, actual);

        digits.push('7');
        digits.push('3');
        digits.push('1');
        digits.push('9');
        digits.push('0');
        digits.push('0');
        digits.push('0');

        expected = 7319000;
        actual = convertCharsToInt(digits);

        assertEquals(expected, actual);

    }
}
