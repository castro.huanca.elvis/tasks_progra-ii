package task2808.operations;

import org.junit.jupiter.api.Test;
import task2808.Calculator;

import static org.junit.jupiter.api.Assertions.assertEquals;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class PowerTest {

    Calculator calculator = new Calculator();

    @Test
    void powerTest() {

        int expected = 32;
        int actual = calculator.calculate("2^5 ");

        assertEquals(expected, actual);

        expected = 125;
        actual = calculator.calculate("5^3 ");

        assertEquals(expected, actual);
    }
}
