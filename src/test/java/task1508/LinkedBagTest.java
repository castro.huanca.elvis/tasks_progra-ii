package task1508;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class LinkedBagTest {

    Bag<String> linkedBagString = new LinkedBag<>();

    @Test
    void selectionSortTest() {
        linkedBagString.add("1");
        linkedBagString.add("2");
        linkedBagString.add("3");
        linkedBagString.add("4");
        linkedBagString.add("5");
        linkedBagString.add("6");

        assertEquals("(6)root={data=6, sig->{data=5, sig->{data=4, " +
                "sig->{data=3, sig->{data=2, sig->{data=1, sig->null}}}}}}", linkedBagString.toString());

        linkedBagString.selectionSort();

        assertEquals("(6)root={data=1, sig->{data=2, sig->{data=3, " +
                "sig->{data=4, sig->{data=5, sig->{data=6, sig->null}}}}}}", linkedBagString.toString());

    }

    @Test
    void bubbleSortTest() {
        linkedBagString.add("1");
        linkedBagString.add("2");
        linkedBagString.add("3");
        linkedBagString.add("4");
        linkedBagString.add("5");
        linkedBagString.add("6");

        assertEquals("(6)root={data=6, sig->{data=5, sig->{data=4, " +
                "sig->{data=3, sig->{data=2, sig->{data=1, sig->null}}}}}}", linkedBagString.toString());

        linkedBagString.bubbleSort();

        assertEquals("(6)root={data=1, sig->{data=2, sig->{data=3, " +
                "sig->{data=4, sig->{data=5, sig->{data=6, sig->null}}}}}}", linkedBagString.toString());

    }
}
