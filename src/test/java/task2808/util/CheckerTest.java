package task2808.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static task2808.util.Checker.*;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class CheckerTest {

    Checker checker = new Checker();

    @Test
    void isDigitTest() {

        boolean digit = isDigit('0');
        assertTrue(digit);

        digit = isDigit('1');
        assertTrue(digit);

        digit = isDigit('2');
        assertTrue(digit);

        digit = isDigit('3');
        assertTrue(digit);

        digit = isDigit('4');
        assertTrue(digit);

        digit = isDigit('5');
        assertTrue(digit);

        digit = isDigit('6');
        assertTrue(digit);

        digit = isDigit('7');
        assertTrue(digit);

        digit = isDigit('8');
        assertTrue(digit);

        digit = isDigit('9');
        assertTrue(digit);

        digit = isDigit('a');
        assertFalse(digit);

        digit = isDigit('b');
        assertFalse(digit);

        digit = isDigit('c');
        assertFalse(digit);

        digit = isDigit('d');
        assertFalse(digit);

        digit = isDigit(',');
        assertFalse(digit);
    }

    @Test
    void isOperatorTest() {

        boolean operator = isOperator('!');
        assertTrue(operator);

        operator = isOperator('+');
        assertTrue(operator);

        operator = isOperator('*');
        assertTrue(operator);

        operator = isOperator('^');
        assertTrue(operator);

        operator = isOperator('a');
        assertFalse(operator);

        operator = isOperator('J');
        assertFalse(operator);

        operator = isOperator('3');
        assertFalse(operator);

        operator = isOperator('1');
        assertFalse(operator);
    }

    @Test
    void validatorRange(){

        assertTrue(checker.verifyRange(523));
        assertTrue(checker.verifyRange(33));
        assertTrue(checker.verifyRange(12321314));
        assertTrue(checker.verifyRange(32432552));
        assertTrue(checker.verifyRange(3453));
        assertTrue(checker.verifyRange(5233));
        assertTrue(checker.verifyRange(876));
        assertTrue(checker.verifyRange(987));
        assertTrue(checker.verifyRange(123123));
        assertTrue(checker.verifyRange(54321));
        assertTrue(checker.verifyRange(3213));
        assertTrue(checker.verifyRange(1231253));
        assertTrue(checker.verifyRange(4353));
        assertTrue(checker.verifyRange(385763));
    }

    @Test
    void validatorTypeVariable(){

        assertFalse(checker.verifyTypeVariable("."));
        assertFalse(checker.verifyTypeVariable(","));
        assertTrue(checker.verifyTypeVariable(":"));
        assertTrue(checker.verifyTypeVariable(";"));
        assertTrue(checker.verifyTypeVariable("'"));

    }

    @Test
    void validatorBrackets(){

        assertTrue(checker.verifyBrackets("()"));
        assertTrue(checker.verifyBrackets("("));
        assertTrue(checker.verifyBrackets(")"));
    }

    @Test
    void validatorNotLetters(){

        assertTrue(checker.verifyNotLetters("a"));
        assertTrue(checker.verifyNotLetters("b"));
        assertTrue(checker.verifyNotLetters("c"));
        assertTrue(checker.verifyNotLetters("S"));
        assertTrue(checker.verifyNotLetters("Z"));
        assertTrue(checker.verifyNotLetters("H"));
        assertTrue(checker.verifyNotLetters("B"));
        assertTrue(checker.verifyNotLetters("x"));
        assertTrue(checker.verifyNotLetters("l"));
        assertFalse(checker.verifyNotLetters("2"));
        assertFalse(checker.verifyNotLetters("12"));
        assertFalse(checker.verifyNotLetters("321"));
        assertFalse(checker.verifyNotLetters("41"));
        assertFalse(checker.verifyNotLetters("*"));
        assertFalse(checker.verifyNotLetters("-"));
        assertFalse(checker.verifyNotLetters("+"));
        assertFalse(checker.verifyNotLetters("?"));
        assertFalse(checker.verifyNotLetters("¿"));
        assertFalse(checker.verifyNotLetters("!"));
        assertFalse(checker.verifyNotLetters(":"));
    }

}
