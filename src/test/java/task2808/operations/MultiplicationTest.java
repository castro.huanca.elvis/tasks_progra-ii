package task2808.operations;

import org.junit.jupiter.api.Test;
import task2808.Calculator;

import static org.junit.jupiter.api.Assertions.assertEquals;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class MultiplicationTest {

    Calculator calculator = new Calculator();

    @Test
    void twoOperantsMultTest() {

        int expected = 8;
        int actual = calculator.calculate("4*2 ");

        assertEquals(expected, actual);

        expected = 25;
        actual = calculator.calculate("5*5 ");

        assertEquals(expected, actual);

        expected = 100;
        actual = calculator.calculate("10*10 ");

        assertEquals(expected, actual);
    }

    @Test
    void severalOperationsMultTest() {

        int expected = 125;
        int actual = calculator.calculate("5*5*5 ");

        assertEquals(expected, actual);

        expected = 135;
        actual = calculator.calculate("5*3*9 ");

        assertEquals(expected, actual);
    }
}
