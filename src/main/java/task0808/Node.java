package task0808;

//STUDENT: ELVIS JOSE CASTRO HUANCA
class Node {

    public int data;
    public Node next = null;

    public static int getNth(Node n, int index) throws Exception{
        while(index > 0){
            n = n.next;
            index --;
        }
        return n.data;
    }

}