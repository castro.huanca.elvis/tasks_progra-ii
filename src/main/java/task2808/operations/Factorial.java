package task2808.operations;

import task2808.exceptions.OperationException;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class Factorial implements Calculable{

    @Override
    public int perform(int left, int right) {
        int factorial = 1;
        while(left != 0) {
            factorial *= left;
            left--;
        }
        if(factorial < 0){
            throw new OperationException("Out of range.");
        }
        return factorial;
    }

    @Override
    public char getSymbol() {
        return '!';
    }

    @Override
    public int getPriority() {
        return 4;
    }

}
