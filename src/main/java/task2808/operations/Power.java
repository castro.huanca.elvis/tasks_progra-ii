package task2808.operations;

import task2808.exceptions.OperationException;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class Power implements Calculable {

    @Override
    public int perform(int left, int right) {
        int numberAux = left;
        if(right == 0) return 1;

        while(right > 1) {
            left *= numberAux;
            right--;
        }

        if(left < 0) {
            throw new OperationException("Out of range.");
        }
        return left;
    }

    @Override
    public char getSymbol() {
        return '^';
    }

    @Override
    public int getPriority() {
        return 3;
    }

}
