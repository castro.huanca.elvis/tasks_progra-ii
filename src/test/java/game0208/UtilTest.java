package game0208;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class UtilTest {

    @Test
    void isWithinRangeTest() {

        assertTrue(Util.isWithinRange(10, 6));
        assertTrue(Util.isWithinRange(10, 9));

        assertFalse(Util.isWithinRange(10, 10));
        assertFalse(Util.isWithinRange(10, 15));
    }

    @Test
    void generateRandomPositionsTest() {
        int[] positions = Util.generateRandomPositions(new String[10][10]);
        boolean areWithinRange = (positions[0] >= 0 && positions[0]<= 9)
                && (positions[1] >= 0 && positions[1]<=9);

        assertTrue(areWithinRange);
    }

    @Test
    void generateRandomPositionXTest() {
        int x = Util.generateRandomPositionX(new String[10][10]);
        boolean isWithinRange = x >= 0 && x <= 9;
        assertTrue(isWithinRange);
    }

    @Test
    void generateRandomPositionYTest() {
        int y = Util.generateRandomPositionY(new String[10][10]);
        boolean isWithinRange = y >= 0 && y <= 9;
        assertTrue(isWithinRange);
    }

    @Test
    void containsTest() {

        int[][] arrays = new int[][] {{1, 2}, {3, 4}, {5, 6}};

        assertTrue(Util.contains(arrays, new int[]{1, 2}));
        assertTrue(Util.contains(arrays, new int[]{5, 6}));
    }
}
