package game0208;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class Player {

    private static final int MINIMUM_POSITION = 0;
    public static final String LETTER_LEFT = "A";
    public static final String LETTER_RIGHT = "D";
    public static final String LETTER_UP = "W";
    public static final String LETTER_DOWN = "S";

    private String name;
    private char nickname;
    private int positionX, positionY;

    public Player(String name, char nickname, int positionX, int positionY) {
        this.name = name;
        this.nickname = nickname;
        this.positionX = positionX;
        this.positionY = positionY;
    }

    public int moveHorizontally(String[][] board, Movement movement){
        int positionMax = board[0].length - 1;
        if( (positionX == MINIMUM_POSITION && movement == Movement.LEFT) || (positionX == positionMax && movement == Movement.RIGHT) ){
            return positionX;
        }
        else if( movement == Movement.LEFT ){
            moveToLeft();
        }
        else if( movement == Movement.RIGHT){
            moveToRight();
        }
        return positionX;
    }

    public int moveVertically(String[][] board, Movement movement){
        int positionMax = board.length;
        if( (positionY == positionMax && movement == Movement.DOWN) || (positionY == MINIMUM_POSITION && movement == Movement.UP) ){
            return positionY;
        }
        else if( movement == Movement.UP ){
            moveToUp();
        }
        else if( movement == Movement.DOWN){
            moveToDown();
        }
        return positionY;
    }

    private void moveToLeft(){
        positionX -= 1;
    }

    private void moveToRight(){
        positionX += 1;
    }

    private void moveToUp(){
        positionY -= 1;
    }

    private void moveToDown(){
        positionY += 1;
    }


    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    public void setPositionY(int positionY) {
        this.positionY = positionY;
    }


    public String[][] move(String[][] board, String letter) {

        if(letter.equalsIgnoreCase(LETTER_UP) ){
            positionY = moveVertically(board, Movement.UP);
        }
        else if(letter.equalsIgnoreCase(LETTER_DOWN)){
            positionY = moveVertically(board, Movement.DOWN);
        }
        else if(letter.equalsIgnoreCase(LETTER_LEFT)){
            positionX = moveHorizontally(board, Movement.LEFT);
        }
        else if(letter.equalsIgnoreCase(LETTER_RIGHT)){
            positionX = moveHorizontally(board, Movement.RIGHT);
        }

        board[positionY][positionX] = String.valueOf(nickname);

        return board;
    }
}
