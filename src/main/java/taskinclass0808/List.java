package taskinclass0808;

public class List {

    Node raiz;

    void remover(int data) {
        Node nodeAuxFinal = searchNode(data);
        Node node = raiz;

        while(node.data != data){
            if(node.data == data){
                raiz.next = node;
            }
            node = node.next;
        }
    }

    Node searchNode(int data){
        Node nodeAux = raiz;
        while(nodeAux.data != data){
            nodeAux = nodeAux.next;
        }
        return nodeAux.next;
    }

    @Override
    public String toString() {
        return "List{" +
                "raiz=" + raiz +
                '}';
    }

    public static void main(String[] args) {
        Node node1 = new Node(1);
        Node node2 = new Node(2);
        Node node3 = new Node(3);
        Node node4 = new Node(4);

        node1.next = node2;
        node2.next = node3;
        node3.next = node4;

        List list = new List();
        list.raiz = node1;
        list.remover(2);

        System.out.println(list);
    }
}
