package task0308;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

//STUDENT: ELVIS JOSE CASTRO HUANCA
public class JUArrayListTest {


    private JUArrayList fillData(JUArrayList juArrayList) {

        Integer integer1 = 1;
        Integer integer2 = 3;
        Integer integer3 = 5;
        Integer integer4 = 10;
        Integer integer5 = 15;
        Integer integer6 = 25;

        juArrayList.add(integer1);
        juArrayList.add(integer2);
        juArrayList.add(integer3);
        juArrayList.add(integer4);
        juArrayList.add(integer5);
        juArrayList.add(integer6);

        return juArrayList;
    }

    @Test
    void addTest() {

        JUArrayList juArrayList = new JUArrayList();

        assertTrue(juArrayList.add(12));
        assertEquals(1, juArrayList.size());

        assertTrue(juArrayList.add(15));
        assertEquals(2, juArrayList.size());

        assertTrue(juArrayList.add(16));
        assertEquals(3, juArrayList.size());

    }

    @Test
    void sizeTest() {
        JUArrayList juArrayList = new JUArrayList();

        assertEquals(0, juArrayList.size());
    }

    @Test
    void isEmptyTest() {
        JUArrayList juArrayList = new JUArrayList();

        assertTrue(juArrayList.isEmpty());

        juArrayList.add(1);
        assertFalse(juArrayList.isEmpty());

        juArrayList.remove(0);
        assertTrue(juArrayList.isEmpty());
    }

    @Test
    void containsTest() {

        JUArrayList juArrayList = new JUArrayList();

        Integer integer1 = 12;
        Integer integer2 = 15;
        Integer integer3 = 17;
        Integer integer4 = 19;
        Integer integer5 = 18;

        juArrayList.add(integer1);
        juArrayList.add(integer2);
        juArrayList.add(integer3);
        juArrayList.add(integer4);
        juArrayList.add(integer5);

        assertTrue(juArrayList.contains(12));
        assertTrue(juArrayList.contains(15));
        assertTrue(juArrayList.contains(17));
        assertTrue(juArrayList.contains(19));
        assertTrue(juArrayList.contains(18));

        assertFalse(juArrayList.contains(1));
        assertFalse(juArrayList.contains(2));
        assertFalse(juArrayList.contains(3));
        assertFalse(juArrayList.contains(4));
        assertFalse(juArrayList.contains(5));

    }

    @Test
    void removeParameterObjectTest(){

        JUArrayList juArrayList = new JUArrayList();

        Integer integer1 = 1;
        Integer integer2 = 3;
        Integer integer3 = 5;
        Integer integer4 = 10;
        Integer integer5 = 15;
        Integer integer6 = 25;

        juArrayList.add(integer1);
        juArrayList.add(integer2);
        juArrayList.add(integer3);

        assertTrue(juArrayList.remove(integer1));
        assertTrue(juArrayList.remove(integer2));
        assertTrue(juArrayList.remove(integer3));

        assertFalse(juArrayList.remove(integer4));
        assertFalse(juArrayList.remove(integer5));
        assertFalse(juArrayList.remove(integer6));

        assertEquals(0 ,juArrayList.size());
    }

    @Test
    void getTest() {
        JUArrayList juArrayList = new JUArrayList();

        juArrayList = fillData(juArrayList);

        assertEquals(1, juArrayList.get(0));
        assertEquals(3, juArrayList.get(1));
        assertEquals(5, juArrayList.get(2));

        assertNull(juArrayList.get(300));
    }

    @Test
    void iteratorTest() {

        JUArrayList juArrayList = new JUArrayList();

        fillData(juArrayList);

        Integer integer2 = 3;
        Integer integer3 = 5;
        Integer integer4 = 10;
        Integer integer5 = 15;
        Integer integer6 = 25;

        assertEquals(integer2, juArrayList.iterator().next());
        assertEquals(integer3, juArrayList.iterator().next());
        assertEquals(integer4, juArrayList.iterator().next());
        assertEquals(integer5, juArrayList.iterator().next());
        assertEquals(integer6, juArrayList.iterator().next());

        assertNull(juArrayList.iterator().next());
    }

    @Test
    void removeParameterIndex() {

        JUArrayList juArrayList = new JUArrayList();

        juArrayList = fillData(juArrayList);

        assertEquals(6, juArrayList.size());

        Integer integer1 = 1;
        Integer integer2 = 3;
        Integer integer3 = 5;

        assertEquals(integer1, juArrayList.remove(0));
        assertEquals(integer2, juArrayList.remove(0));
        assertEquals(integer3, juArrayList.remove(0));

        assertEquals(3, juArrayList.size());
    }

    @Test
    void clearTest() {

        JUArrayList juArrayList = new JUArrayList();

        juArrayList = fillData(juArrayList);
        juArrayList.clear();

        assertEquals(0, juArrayList.size());
        assertNull(juArrayList.get(0));

    }

    @Test
    void addParameterIndexElement() {
        JUArrayList juArrayList = new JUArrayList();

        Integer integer1 = 1;
        Integer integer2 = 3;

        juArrayList = fillData(juArrayList);

        assertEquals(6, juArrayList.size());

        juArrayList.add(1, integer1);
        assertEquals(7, juArrayList.size());
        assertEquals(1, juArrayList.get(1));

        juArrayList.add(0, integer2);
        assertEquals(8, juArrayList.size());
        assertEquals(3, juArrayList.get(0));

    }

    @Test
    void toArrayTest() {

        JUArrayList juArrayList = new JUArrayList();
        juArrayList = fillData(juArrayList);

        Object[] arrayTest = juArrayList.toArray();

        assertEquals(juArrayList.get(0), arrayTest[0]);
        assertEquals(juArrayList.get(1), arrayTest[1]);
        assertEquals(juArrayList.get(2), arrayTest[2]);
        assertEquals(juArrayList.get(3), arrayTest[3]);
        assertEquals(juArrayList.get(4), arrayTest[4]);
        assertEquals(juArrayList.get(5), arrayTest[5]);
    }

    @Test
    void toArrayWithParameterTest() {

        JUArrayList juArrayList = new JUArrayList();
        juArrayList = fillData(juArrayList);

        Object[] arrayTest = new Object[4];

        arrayTest = juArrayList.toArray(arrayTest);
        assertEquals(6, arrayTest.length);

        assertEquals(1, arrayTest[0]);
        assertEquals(3, arrayTest[1]);
        assertEquals(5, arrayTest[2]);
    }
}
